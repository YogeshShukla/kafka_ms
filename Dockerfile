FROM java:8
ENV JAVA_VERSION 8u31
ENV BUILD_VERSION b13
EXPOSE 8081
#install Spring Boot artifact
WORKDIR /
ADD /target/spring-kafka-hello-world-pc-0.0.1-SNAPSHOT.jar spring-kafka-hello-world-pc-0.0.1-SNAPSHOT.jar
RUN sh -c 'touch /spring-kafka-hello-world-pc-0.0.1-SNAPSHOT.jar'
ENTRYPOINT ["java", "-jar", "/spring-kafka-hello-world-pc-0.0.1-SNAPSHOT.jar"]


